package com.neology.control_conducir;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.neology.control_conducir.adapters.PagerAdapter;
import com.neology.control_conducir.tabs.SlidingTabLayout;
import com.neology.control_conducir.utils.ZoomOutPagerTransformer;

public class Data_Online extends AppCompatActivity {

    private ViewPager pager;
    private SlidingTabLayout mTabs;
    String tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_layout);
        tipo = getIntent().getStringExtra("tipo");
        slidingBar_Pager();
    }

    private void slidingBar_Pager() {
        pager = (ViewPager) findViewById(R.id.pagerID);
        pager.setAdapter(new PagerAdapter(getSupportFragmentManager(), getApplicationContext(), tipo));
        pager.setPageTransformer(true, new ZoomOutPagerTransformer());
        /*
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setViewPager(pager);*/
    }
}
