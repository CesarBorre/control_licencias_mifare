package com.neology.control_conducir;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Read_UID_Doc {
    public static JSONObject uid_define;
    public static JSONArray uid_item;
    public static void createJsonStructure(InputStream rawDoc) {
        try {
            uid_define = new JSONObject(readJsonFile(rawDoc));
            uid_item = uid_define.getJSONArray("uid_definidos");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private static String readJsonFile(InputStream is) {
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }
}