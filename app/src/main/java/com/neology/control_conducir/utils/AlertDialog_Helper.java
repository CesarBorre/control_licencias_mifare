package com.neology.control_conducir.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.neology.control_conducir.R;

/**
 * Created by LeviAcosta on 11/10/2016.
 */

public class AlertDialog_Helper {

    public static AlertDialog showAlert(String tipoLicencia, Context context) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder alert;
        String title = null;
        String msg = null;
        if (tipoLicencia.equals("2")) {
            title = "LICENCIA VENCIDA";
            msg = "LA LICENCIA DE CONDUCIR VEHICULOS MENORES HA VENCIDO, POR FAVOR DE VERIFICAR";
        } else {
            title = "CARNE VENCIDO";
            msg = "EL CARNE DE EDUCACION Y SEGURIDAD VIAL HA VENCIDO, POR FAVOR DE VERIFICAR";
        }
        alert = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setIcon(R.drawable.warning)
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
        alertDialog = alert.create();
        return alertDialog;
    }
}
