package com.neology.control_conducir.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.neology.control_conducir.R;

/**
 * Created by root on 9/09/16.
 */
public class FolioData_Dialog extends DialogFragment {
    int position;

    TextView nombreTitulado, carreraTitulado, numIDTitulado;
    String [] nombre, carrera, numID;

    public static FolioData_Dialog newInstance(int position) {
        FolioData_Dialog f = new FolioData_Dialog();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.folio_data_dialog, null);
        nombreTitulado = (TextView)v.findViewById(R.id.nombreExID);
        carreraTitulado = (TextView)v.findViewById(R.id.carreraExID);
        numIDTitulado = (TextView)v.findViewById(R.id.numIdentificacionID);

/*
        nombre = getActivity().getResources().getStringArray(R.array.tipo_titulos);
        carrera = getActivity().getResources().getStringArray(R.array.carrera);
        numID = getActivity().getResources().getStringArray(R.array.numId);
        nombreTitulado.setText(nombre[position]);
        carreraTitulado.setText(carrera[position]);
        numIDTitulado.setText(numID[position]);
        */

        return new AlertDialog.Builder(getContext())
                .setTitle("DATOS PERSO")
                .setIcon(R.mipmap.logo)
                .setView(v)
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }
}
