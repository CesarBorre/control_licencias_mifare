package com.neology.control_conducir.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.neology.control_conducir.Grabar_UID;
import com.neology.control_conducir.R;

/**
 * Created by root on 9/09/16.
 */
public class Settings_Dialog extends DialogFragment {
    EditText usr;
    EditText pwd;

    public static Settings_Dialog newInstance() {
        Settings_Dialog f = new Settings_Dialog();
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.settings_dialog, null);
        usr = (EditText)v.findViewById(R.id.usrID);
        pwd = (EditText)v.findViewById(R.id.pwdID);

        return new AlertDialog.Builder(getContext())
                .setTitle("CONFGURACIÓN")
                .setIcon(R.mipmap.logo)
                .setView(v)
                .setPositiveButton("INGRESAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                changeIP();
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }

    private void changeIP() {
        if(usr.getText().length()>0&&pwd.getText().length()>0) {
            if(usr.getText().toString().equals("NEOLOGY")&&pwd.getText().toString().equals("NeologyNFC")){
                Intent intent = new Intent(getActivity().getApplicationContext(), Grabar_UID.class);
                startActivity(intent);
                getActivity().finish();
                dismiss();
            } else {
                Toast.makeText(getContext(), "Datos inválidos", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Ingresar datos completos", Toast.LENGTH_SHORT).show();
        }
    }
}
