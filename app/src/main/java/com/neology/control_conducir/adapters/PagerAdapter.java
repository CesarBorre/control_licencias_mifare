
package com.neology.control_conducir.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.neology.control_conducir.subfragments.Frente_Fragment;
import com.neology.control_conducir.subfragments.Reverso_Fragment;

/**
 * Created by Cesar on 02/06/2016.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    Context c;
    String tabs[] = {"TITULO", "CERTIFICADO"};
    String tipo = null;
    public PagerAdapter(FragmentManager fm, Context c, String tipo) {
        super(fm);
        this.c = c;
        this.tipo = tipo;
    }
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                Bundle args = new Bundle();
                args.putString("tipo", tipo);
                fragment = new Frente_Fragment();
                fragment.setArguments(args);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putString("tipo", tipo);
                fragment = new Reverso_Fragment();
                fragment.setArguments(bundle);
                break;
        }
        return fragment;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
//        Drawable drawable = ContextCompat.getDrawable(c, R.mipmap.ic_launcher);
//        drawable.setBounds(0, 0, 45, 45);
//        ImageSpan imageSpan = new ImageSpan(drawable);
//        SpannableString spannableString = new SpannableString("  ");
//        spannableString.setSpan(imageSpan, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        return spannableString;
    }
    @Override
    public int getCount() {
        return 2;
    }
}
