package com.neology.control_conducir.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.neology.control_conducir.MainActivity;
import com.neology.control_conducir.R;


/**
 * Created by root on 9/09/16.
 */
public class ConfirmRecord_Dialog extends DialogFragment {

    public static ConfirmRecord_Dialog newInstance() {
        ConfirmRecord_Dialog f = new ConfirmRecord_Dialog();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate( R.layout.confirm_record_dialog, null);

        return new AlertDialog.Builder(getContext())
                .setTitle("PERSO TAG")
                .setIcon(R.mipmap.logo)
                .setView(v)
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity)getActivity()).showDialog();
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }
}
