package com.neology.control_conducir;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.neology.control_conducir.utils.AlertDialog_Helper;
import com.neology.control_conducir.utils.CheckInternetConnection;
import com.neology.control_conducir.utils.SnackBar;

public class DataActivity extends AppCompatActivity {
    String [] datos = null;
    TextView tipoLic, nombre, numDoc;
    ImageView img;
    String tipo;
    Button intentarBtn;
    CoordinatorLayout backGround;
    ImageView warning1, warning2, banner1, banner2;
    TextView vencido, vencido1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        datos = getIntent().getStringExtra("UID").split("\\|");
        tipo = datos[2];
        initElements(datos);
        setToolbar();
        if(datos[1].equals("2")) {
            AlertDialog_Helper.showAlert(datos[2], DataActivity.this).show();
            backGround.setBackgroundResource(R.drawable.warning_back);
            banner1.setImageResource(R.drawable.banner_warning);
            banner2.setImageResource(R.drawable.banner_warning);
            warning1.setVisibility(View.VISIBLE);
            warning2.setVisibility(View.VISIBLE);
            vencido.setVisibility(View.VISIBLE);
        }
    }

    private void setToolbar() {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    private void initElements(String... params) {
        nombre = (TextView)findViewById(R.id.nombreID);
        tipoLic = (TextView)findViewById(R.id.tipoLicenciaID);
        nombre = (TextView)findViewById(R.id.nombreID);
        numDoc = (TextView)findViewById(R.id.numDocID);
        img = (ImageView)findViewById(R.id.image_paralax);
        intentarBtn = (Button) findViewById(R.id.intentarBtnID);
        backGround = (CoordinatorLayout)findViewById(R.id.coordinator);
        warning1 = (ImageView)findViewById(R.id.warning1ID);
        warning2 = (ImageView)findViewById(R.id.warning2ID);
        banner1 = (ImageView)findViewById(R.id.banner1);
        banner2 = (ImageView)findViewById(R.id.banner2);
        vencido = (TextView)findViewById(R.id.vencidoID);
        vencido1 = (TextView)findViewById(R.id.vencido1ID);

        if (params[2].equals("2")) {
            img.setImageResource(R.drawable.vehiculos_menores_logo);
            tipoLic.setText("LICENCIA DE CONDUCIR VEHICULOS MENORES");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.buscar_item:
                searchData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void searchData() {
        if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
            Intent i = new Intent(getApplicationContext(), Data_Online.class);
            i.putExtra("tipo", tipo);
            startActivity(i);
        } else {
            SnackBar.showSnackBar(getResources().getString(R.string.no_internet), this, 1, R.id.coordinator);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
