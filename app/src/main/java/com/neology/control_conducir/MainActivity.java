package com.neology.control_conducir;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.neology.control_conducir.dialogs.Settings_Dialog;
import com.neology.control_conducir.utils.Constants;
import com.nxp.nfclib.classic.IMFClassic;
import com.nxp.nfclib.classic.IMFClassicEV1;
import com.nxp.nfclib.exceptions.CloneDetectedException;
import com.nxp.nfclib.exceptions.ReaderException;
import com.nxp.nfclib.exceptions.SmartCardException;
import com.nxp.nfclib.ntag.INTag213215216;
import com.nxp.nfclib.utils.Utilities;
import com.nxp.nfcliblite.NxpNfcLibLite;
import com.nxp.nfcliblite.Nxpnfcliblitecallback;
import com.nxp.nfcliblite.cards.IPlus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    public static String TAG = MainActivity.class.getSimpleName();
    private NxpNfcLibLite libInstance = null;

    boolean resLectura = false;

    int iConta = 0;
    /**
     * Mifare MFClassic instance initiated.
     */
    private IMFClassic classic;
    /**
     * Mifare Plus instance initiated.
     */
    private IPlus plus;

    private enum EnumCardType {
        EnumPlus,
        EnumClassicEV1,
        EnumNone
    }

    SharedPreferences sharedPreferences;
    ProgressDialog dialog;

    private EnumCardType mCardType = EnumCardType.EnumNone;

    private boolean mIsPerformingCardOperations = false;

    WebView wv;
    RelativeLayout errorTag;
    Button intentarBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        libInstance = NxpNfcLibLite.getInstance();
        libInstance.registerActivity(this);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_NAME, MODE_PRIVATE);
        initProgressDialog();
        loadWebView();

        InputStream uid_doc = getResources().openRawResource(R.raw.uid);
        Read_UID_Doc.createJsonStructure(uid_doc);

        errorTag = (RelativeLayout)findViewById(R.id.tagErrorID);
        intentarBtn = (Button)findViewById(R.id.intentarBtnID);
    }

    private void loadWebView() {
        wv = (WebView) findViewById(R.id.webViewMain);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/tap.html");
    }

    private void initProgressDialog() {
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Leyengo TAG");
        dialog.setMessage("Espere, por favor...");
        dialog.setCanceledOnTouchOutside(false);
    }

    public void showDialog() {
        DialogFragment dialogFragment = Settings_Dialog.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        resLectura = false;
        iConta = 0;
        dialog.show();
        if (intent != null) {
            try {
                libInstance.filterIntent(intent, new Nxpnfcliblitecallback() {

                    @Override
                    public void onClassicEV1CardDetected(IMFClassicEV1 imfClassicEV1) {
                        if (mCardType == EnumCardType.EnumClassicEV1 && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumClassicEV1;
                        classic = imfClassicEV1;
                /* Insert your logic here by commenting the function call below. */
                        try {
                            classic.getReader().connect();
                            classicEV1CardLogic();
                        } catch (Throwable t) {
                            Log.e(TAG, "Unknown Error Tap Again!");
                        }
                        mIsPerformingCardOperations = false;
                    }

                    @Override
                    public void onNTag213215216CardDetected(INTag213215216 inTag213215216) {
                        Log.d(TAG, "Card detected inTag213215216");
                    }

                    @Override
                    public void onPlusCardDetected(final IPlus iPlus) {
                        if (mCardType == EnumCardType.EnumPlus && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumPlus;

                        plus = iPlus;
                        try {
                            plus.getReader().connect();
                            //                        plusCardLogic();
                            iConta++;
                            if (Utilities.dumpBytes(plus.getCardDetails().uid) != null) {
                                Log.i(TAG, "Card Detected: " + plus.getCardDetails().cardName + " "
                                        + plus.getCardDetails().securityLevel + Utilities.dumpBytes(plus.getCardDetails().uid));
                                if (iConta == 2) {
                                    read_UIDjson(Read_UID_Doc.uid_item, Utilities.dumpBytes(plus.getCardDetails().uid));
                                    resLectura = true;
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                            }

                            plus.getReader().close();
                        } catch (ReaderException e1) {
                            e1.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        } catch (Throwable t) {
                            t.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        }
                        mIsPerformingCardOperations = false;

                    }
                });
            } catch (CloneDetectedException e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Intentar de nuevo", Toast.LENGTH_SHORT).show();
        }

        if (!resLectura) {
            Toast.makeText(getApplicationContext(), "Intentar lectura de nuevo", Toast.LENGTH_SHORT).show();
        }
    }

    public void classicEV1CardLogic() throws SmartCardException {
        Log.i(TAG, "Card Detected : " + classic.getCardDetails().cardName);
        try {
            read_UIDjson(Read_UID_Doc.uid_item, Utilities.dumpBytes(classic.getCardDetails().uid));
            resLectura = true;
            dialog.dismiss();
            classic.getReader().close();
        } catch (ReaderException e) {
            e.printStackTrace();
        }
    }

    private void read_UIDjson(JSONArray array, String UID) {
        boolean resultado = false;
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                String uid = jsonObject.getString("uid");
                String folio = jsonObject.getString("folio");
                String lic = jsonObject.getString("lic");
                Log.d(TAG, "UID JSON " + uid);
                Log.d(TAG, "folio JSON " + folio);
                Log.d(TAG, "lic JSON " + lic);
                if (uid.equals(UID)) {
                    //mostrar contenido
                    mostrarContenido(uid+"|"+folio+"|"+lic);
                    resultado = true;
                    break;
                } else {
                    resultado = false;
                }
            }
        } catch (Exception e) {

        }
        dialog.dismiss();
        if (!resultado) {
            //Toast.makeText(getApplicationContext(), "EL TAG NO CORRESPONDE A LA APLICACIÓN", Toast.LENGTH_SHORT).show();
            mostrarError();
        }
    }

    private void mostrarContenido(String UID) {
        Intent intent = new Intent(getApplicationContext(), DataActivity.class);
        intent.putExtra("UID", UID);
        startActivity(intent);
    }

    private void mostrarError() {
        wv.setVisibility(View.GONE);
        errorTag.setVisibility(View.VISIBLE);
        intentarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regresarInicio();
            }
        });
    }

    private void regresarInicio() {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
