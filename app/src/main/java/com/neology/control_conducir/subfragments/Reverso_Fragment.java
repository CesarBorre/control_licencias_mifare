package com.neology.control_conducir.subfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neology.control_conducir.R;

/**
 * Created by LeviAcosta on 07/10/2016.
 */

public class Reverso_Fragment extends Fragment {
    String tipo = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipo = getArguments().getString("tipo");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v;
        if(!tipo.equals("2")) {
            v = inflater.inflate(R.layout.reverso_fragment, container, false);
        } else {
            v = inflater.inflate(R.layout.reverso_fragment2, container, false);
        }
        return v;
    }
}
